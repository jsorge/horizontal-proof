//
//  ImageCollectionViewCell.swift
//  Horizontal Proof
//
//  Created by Jared Sorge on 6/14/16.
//  Copyright © 2016 Jared Sorge. All rights reserved.
//

import UIKit

enum Image: String {
    case cow = "cow"
    case fire = "fire"
}

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet private var imageView: UIImageView!
    
    func confifure(withImage image: Image) {
        imageView.image = UIImage(named: image.rawValue)
    }

}
