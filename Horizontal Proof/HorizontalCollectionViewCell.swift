//
//  HorizontalCollectionViewCell.swift
//  Horizontal Proof
//
//  Created by Jared Sorge on 6/14/16.
//  Copyright © 2016 Jared Sorge. All rights reserved.
//

import UIKit

class HorizontalCollectionViewCell: UICollectionViewCell {

    @IBOutlet private var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.registerNib(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.reloadData()
    }

}

extension HorizontalCollectionViewCell: UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ImageCollectionViewCell", forIndexPath: indexPath) as! ImageCollectionViewCell
        cell.confifure(withImage: .fire)
        return cell
    }
}


extension HorizontalCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2, height: collectionView.frame.size.height)
    }
}
