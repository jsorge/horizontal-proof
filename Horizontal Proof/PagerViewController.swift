//
//  PagerViewController.swift
//  Horizontal Proof
//
//  Created by Jared Sorge on 6/14/16.
//  Copyright © 2016 Taphouse Software. All rights reserved.
//

import UIKit

class PagerViewController: UIViewController {

    var pages = [UIViewController]()
    let pageController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let storyboard = storyboard else { return }
        pages.append((storyboard.instantiateViewControllerWithIdentifier("ViewController")))
        pages.append((storyboard.instantiateViewControllerWithIdentifier("ViewController")))
        pages.append((storyboard.instantiateViewControllerWithIdentifier("ViewController")))
        pages.append((storyboard.instantiateViewControllerWithIdentifier("ViewController")))
        
        pageController.dataSource = self
        
        pageController.setViewControllers([pages.first!], direction: .Forward, animated: true, completion: nil)
        
        pageController.view.frame = view.bounds
        addChildViewController(pageController)
        view.addSubview(pageController.view)
        pageController.didMoveToParentViewController(self)
    }
}

extension PagerViewController: UIPageViewControllerDataSource {
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.indexOf(viewController)!
        guard currentIndex > 0 else { return nil }
        
        let destIndex = currentIndex - 1
        return pages[destIndex]
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.indexOf(viewController)!
        guard currentIndex + 1 < pages.count else { return nil }
        
        let destIndex = currentIndex + 1
        return pages[destIndex]
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pages.count
    }
}